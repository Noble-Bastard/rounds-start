<?php

namespace app\Data;

class StartRoundDTO
{
    public string $roundId;
    public string $playerId;

    /**
     * @param string $roundId
     * @param string $playerId
     */
    public function __construct(string $roundId, string $playerId)
    {
        $this->roundId = $roundId;
        $this->playerId = $playerId;
    }
}