<?php

namespace app\Data;

class EndRoundDTO
{
    public string $roundId;
    public int $reward;

    /**
     * @param string $roundId
     * @param int $reward
     */
    public function __construct(string $roundId, int $reward)
    {
        $this->roundId = $roundId;
        $this->reward = $reward;
    }

}