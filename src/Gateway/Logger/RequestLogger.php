<?php

namespace app\Gateway\Logger;

class RequestLogger
{
    /**
     * @param string $roundType
     * @param string $roundId
     * @param string|int $item
     * @param array $data
     * @return void
     */
    public static function log(string $roundType, string $roundId, $item, array $data): void
    {
        $logMessage = sprintf(
            "[%s] Request to %s with round_id=%s, item=%s returned status_code=%s, response_body=%s",
            date('Y-m-d H:i:s'),
            $roundType,
            $roundId,
            $item,
            $data['success'],
            json_encode($data)
        );
        echo $logMessage . PHP_EOL;
    }

    /**
     * @param string $roundType
     * @param string $data - request body
     * @param string $errorMessage
     * @return void
     */
    public static function errorLog(string $roundType, string $data, string $errorMessage): void
    {
        $logMessage = sprintf(
            "[%s] Request to %s was failed, response_body=%s, error_message=%s",
            date('Y-m-d H:i:s'),
            $roundType,
            $data,
            $errorMessage
        );
        echo $logMessage . PHP_EOL;
    }
}