<?php
declare(strict_types=1);

namespace app\Gateway;

use Exception;
use app\Data\EndRoundDTO;
use app\Data\StartRoundDTO;
use app\Gateway\Logger\RequestLogger;

class OnlyPlayGateway
{
    private const BASE_URL = 'https://int.dev.onlyplay.net/test_api/';
    private int $providerId;
    private string $key;

    /**
     * @param int $providerId
     * @param string $key
     */
    public function __construct(int $providerId, string $key)
    {
        $this->providerId = $providerId;
        $this->key = $key;
    }

    /**
     * Так как метод маленький, не вижу смысла его переносить в хелперы/отдельный класс
     * @param string $data is request body
     * @return string
     */
    private function sign(string $data): string
    {
        return hash_hmac('sha256', $data, $this->key);
    }

    /**
     * @param string $endpoint
     * @param string $data
     * @return array|null
     * @throws Exception
     */
    private function sendRequest(string $endpoint, string $data): array
    {
        $url = self::BASE_URL . $endpoint;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);

        $response = curl_exec($ch);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
            RequestLogger::errorLog($endpoint, $data, "response is not 200");
            $this->throw($response);
        }

        curl_close($ch);

        $data = json_decode($response, true);
        if ($data['success'] !== true) {
            RequestLogger::errorLog($endpoint, $data, "response is not success");
            $this->throw($response);
        }
        return $data;
    }

    /**
     * Тоже самое с sign, думаю переносить в отдельные сервисы не стоит
     * @param StartRoundDTO $startRoundDTO
     * @return array
     * @throws Exception
     */
    public function startRound(StartRoundDTO $startRoundDTO): array
    {
        $data = [
            'round_id' => $startRoundDTO->roundId,
            'provider_id' => $this->providerId,
            'player_id' => $startRoundDTO->playerId
        ];
        $data['sign'] = $this->sign(json_encode($data));
        $data = $this->sendRequest('start_round', json_encode($data));
        RequestLogger::log("start_round", $startRoundDTO->roundId, $startRoundDTO->playerId, $data);
        return $data;
    }

    /**
     * @param EndRoundDTO $endRoundDTO
     * @return array
     * @throws Exception
     */
    public function endRound(EndRoundDTO $endRoundDTO): array
    {
        $data = [
            'round_id' => $endRoundDTO->roundId,
            'provider_id' => $this->providerId,
            'reward' => $endRoundDTO->reward
        ];
        $data['sign'] = $this->sign(json_encode($data));
        $data = $this->sendRequest('end_round', json_encode($data));
        RequestLogger::log("end_round", $endRoundDTO->roundId, $endRoundDTO->reward, $data);
        return $data;

    }

    /**
     * @throws Exception
     */
    private function throw(string $response)
    {
        throw new Exception($response);
    }
}
