## Install composer
```angular2html
composer install
```
## Check creds in 
```config/config.php```
## Run project
```make run```

## Or
```
php public/index.php 
```