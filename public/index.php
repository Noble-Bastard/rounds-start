<?php

$config = require('config/config.php');

require_once('vendor/autoload.php');

use app\Data\EndRoundDTO;
use app\Data\StartRoundDTO;
use app\Gateway\OnlyPlayGateway;


$api = new OnlyPlayGateway(
    $config['provider_id'],
    $config['key']
);

$api->startRound(
    new StartRoundDTO(
        $config['round_id'],
        $config['start_round_data']['player_id']
    )
);

$api->endRound(
    new EndRoundDTO(
        $config['round_id'],
        $config['end_round_data']['reward']
    )
);
